# create network policy 

kubectl apply -f network-policy.yaml

# update network policy

kubectl edit netpol netpol-1 -n test

```
# Please edit the object below. Lines beginning with a '#' will be ignored,
# and an empty file will abort the edit. If an error occurs while saving this file will be
# reopened with the relevant failures.
#
apiVersion: extensions/v1beta1
kind: NetworkPolicy
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"networking.k8s.io/v1","kind":"NetworkPolicy","metadata":{"annotations":{},"name":"netpol-1","namespace":"test"},"spec":{"podSelector":{},"policyTypes":["Ingress","Egress"]}}
  creationTimestamp: "2020-02-04T18:40:31Z"
  generation: 2
  name: netpol-1
  namespace: test
  resourceVersion: "18013"
  selfLink: /apis/extensions/v1beta1/namespaces/test/networkpolicies/netpol-1
  uid: a54d3d22-35a5-45c7-9856-da1d7c374c57
spec:
  ingress:
  - from:
    - podSelector:
        matchLabels:
          run: client
  podSelector:
    matchLabels:
      run: nginx
  policyTypes:
  - Ingress
```




